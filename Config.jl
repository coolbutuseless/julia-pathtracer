#!/usr/bin/env julia

# There's a lot of config information for a renderer. 
# initially all this information was global, but I made it a type
# and pass it around => decent speedup
# Also, I pre-allocate some space for some vectors which are used lots
# this saves a whole tonne of allocations in intersect() functions
immutable Config
    rows::Int64
    cols::Int64
    sqrt_n_rays_per_pixel::Int64
    maxdepth::Int64
    camera_eye::Vec4d
    image_plane_x::Vec4d
    image_plane_y::Vec4d
    world_00::Vec4d
    objects::Array{Object,1}
    # pre-allocate some arrays
    ray_origin::Vec4d
    ray_direction::Vec4d
    object_normal::Vec4d
    world_normal::Vec4d
    xvec::Vec4d
    yvec::Vec4d
    u::Vec4d
    v::Vec4d
    Li::Vec4d
    path_throughput::Vec4d
    estimate::Vec4d
    contact_point::Vec4d
    wi::Vec4d
    dof::Bool
end

function setup(N, sqrt_n_rays_per_pixel, maxdepth)
    sqrt_n_rays_per_pixel = sqrt_n_rays_per_pixel
    maxdepth = maxdepth
    N = N
    rows, cols = N, N
    camera_eye        = [0.0, 0, -4.0, 1.0]
    camera_up         = [0.0, 1.0, 0.0, 0.0] 
    camera_look_point = [0.0, -2, 2, 1.0]
    image_plane_d     = 4.0
    camera_look       = normalise!(camera_look_point - camera_eye) 
    image_plane_x     = normalise!(crossproduct(camera_up, camera_look))
    image_plane_y     = normalise!(crossproduct(camera_look, image_plane_x))

    world_00 = camera_eye + image_plane_d * camera_look

    # back wall
    object2world = create_transform("xangle", -90) * create_transform("translate", [0.0, 0.0, 3.5]) 
    backwall = HalfSpace([1.0, 1.0, 1.0, 0.0], object2world)

    # ground
    object2world = create_transform("translate", [0.0, -2, 0.0])
    ground = HalfSpace([1.0, 1.0, 1.0, 0.0], object2world)

    # Cube
    object2world = create_transform("yangle", 18) * create_transform("translate", [-0.5, -1.5, 0.5]) 
    #cube = Cube([0.4, 0.4, 0.8, 0.0], object2world)
    cube = Cube([0.4, 0.4, 0.8, 0.0], 0.5, object2world)

    # Rear Cube L
    object2world = create_transform("translate", [-0.5, -1.5, 2.5]) 
    cube_l = Cube([0.4, 0.4, 0.8, 0.0], object2world)

    # Rear Cube R
    object2world = create_transform("translate", [0.75, -1.5, 2]) 
    cube_r = Cube([0.4, 0.4, 0.8, 0.0], object2world)

    # Front cube
    object2world = create_transform("scale", 0.5) * create_transform("translate", [-0.8, -1.5, -0.65]) 
    cube_front = Cube([0.4, 0.4, 0.8, 0.0], false, object2world)

    # Front sphere
    object2world = create_transform("scale", 0.75) * create_transform("translate", [-0.2, -0.75, -0.65]) 
    sphere_front = Sphere([0.4, 0.4, 0.8, 0.0], true, object2world)

    # sphere
    object2world = create_transform("scale", 1.3) * create_transform("translate", [0.7, -1.5, 0.2]) 
    sphere = Sphere([1, 0.7, 0.7, 0], object2world)

    # Light
    object2world = create_transform("translate", [0.0, 1.999, 0.0])
    light = Sphere([1.0, 1.0, 1.0, 0.0], object2world, true) 

    # ================== CSG =========================
    # Cube
    object2world = create_transform()
    cube2 = Cube([0.4, 0.4, 0.8, 0.0], object2world)

    # sphere
    object2world = create_transform("scale", 1.3)
    sphere2 = Sphere([1, 0.7, 0.7, 0], object2world)
    
    # CSG
    object2world = create_transform("translate", [0.7, -1.5, 0.2])
    csg = CSG([0.0, 0.0, 0.0, 0.0], object2world, (cube2, MERGE_DIFFERENCE, sphere2))
    prepare_CSG_objects(csg)

    objects = [ground, csg, light, cube, backwall, cube_l, cube_r, cube_front]
    #println(typeof(objects))

    u = [0.0, 0.0, 0.0, 0.0]
    v = [0.0, 0.0, 0.0, 0.0]
    tmp1 = [0.0, 0.0, 0.0, 0.0]
    tmp2 = [0.0, 0.0, 0.0, 0.0]
    tmp3 = [0.0, 0.0, 0.0, 0.0]
    xvec = [1.0, 0.0, 0.0, 0.0]
    yvec = [0.0, 1.0, 0.0, 0.0]
    estimate = [0.0, 0.0, 0.0, 0.0]
    Li = [0.0, 0.0, 0.0, 0.0]
    path_throughput = [0.0, 0.0, 0.0, 0.0]
    ray_origin = [0.0, 0.0, 0.0, 0.0]
    ray_direction = [0.0, 0.0, 0.0, 0.0]
    object_normal = [0.0, 0.0, 0.0, 0.0]
    world_normal = [0.0, 0.0, 0.0, 0.0]
    contact_point = [0.0, 0.0, 0.0, 0.0]
    wi = [0.0, 0.0, 0.0, 0.0]

    config = Config(rows, cols, sqrt_n_rays_per_pixel, maxdepth, camera_eye, image_plane_x, image_plane_y, world_00, objects,
                    ray_origin, ray_direction,
                    object_normal, world_normal,
                    xvec, yvec, u, v,
                    Li, path_throughput, estimate,
                    contact_point, wi,
                    true) # dof
end
