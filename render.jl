#!/usr/bin/env julia
require("png.jl")
require("Vector.jl")
require("Object.jl")
require("Ray.jl")
require("Config.jl")



function sample_mirror(config::Config,  normal::Vec4d, wo::Vec4d)
    #return wo - normal * 2.0 * dotproduct(normal, wo)
    wi = config.wi
    two_dp = 2.0 * dotproduct(normal, wo)
    for i in 1:3
        wi[i] = wo[i] - normal[i] * two_dp
    end
    return wi, 1.0
end

function sample_diffuse(config::Config, normal::Vec4d, wo::Vec4d)
    # Calculate new ray direction for a diffuse surface
    # i.e. a random ray within the hemisphere defined by the
    # normal to the surface at this point

    # using pre-allocated u, v saves about 4 seconds!
    u = config.u
    v = config.v
    #wi = [0.0, 0.0, 0.0, 0.0]
    wi = config.wi
    for i in 1:3
        wi[i] = 0.0
    end

    # only needed for transparent objs. maybe CSG
    #if dotproduct(normal, wo) > 0
        #neg(normal)
    #end

    r1 = 2.0 * pi * rand()
    r2 = rand()
    r2s = sqrt(r2)

    if abs(normal[1]) > 0.1
        crossproduct!(config.yvec, normal, u)
    else
        crossproduct!(config.xvec, normal, u)
    end
    normalise!(u)
    crossproduct!(normal, u, v)

    #wi = u * cos(r1) * r2s + v * sin(r1) * r2s + normal * sqrt(1-r2)
    # manually devectorize. This had a HUUUGE impact. Went from 27s => 19s
    f1 = cos(r1) * r2s
    f2 = sin(r1) * r2s
    f3 = sqrt(1.0 - r2)
    for i in 1:3
        wi[i] = f1 * u[i] + f2 * v[i] + f3 * normal[i]
    end

    return normalise!(wi), 1.0
end

function estimate_direct_lighting(config::Config, contact_point::Vec4d, normal::Vec4d, light_path)
    estimate = config.estimate
    for i in 1:3
        estimate[i] = 0.0
    end

    for light_info in light_path
        object, light_point, light_vector, e = light_info
        to_light = copy(light_point)
        for i in 1:3
            to_light[i] -= contact_point[i]
        end
        to_light[4] = 0
        normalise!(to_light)  # to_light is now a vector from contact point towards the lightsource

        l = dotproduct(normal, to_light)
        m = dotproduct(normal, light_vector)
        if l > 0.0 && m < 0.0
            light_probe = Ray(contact_point, to_light)
            light_probe_intersect = intersect_scene(config, light_probe)::Intersection
            if light_probe_intersect.object == object
                # manually devectorize
                # estimate += object.colour * l * 1.0/pi
                tmp = l / pi
                for i in 1:3
                    estimate[i] += e[i] * tmp
                end
            end
        end
    end
    estimate
end


function intersect_scene(config::Config, ray::Ray)
    closest_object = nothing
    closest_distance = inf(Float64)
    entering = true
    for object in config.objects
        this_intersect, _ = intersect(config, object, ray)::(Union(Intersection,Nothing),Union(Intersection,Nothing))
        if this_intersect != nothing && this_intersect.distance > 0.0 && (this_intersect.distance < closest_distance)
            closest_object   = this_intersect.object
            closest_distance = this_intersect.distance
            entering         = this_intersect.entering
        end
    end
    Intersection(closest_object, ray, closest_distance, entering)
end

function generate_light_path(config::Config)
    path = (Object, Vec4d, Vec4d, Vec4d)[]
    # MFC FIXME really should be doing a random selection from all lights based upon light power
    light = filter(x -> x.is_light, config.objects)[1]
    point, normal = random_point_and_direction_on_light(config, light)
    e = copy(light.colour)
    push!(path, (light, point, normal, e))

    #Create this light ray and cast it into the scene
    light_ray = Ray(point, normal)
    path2 = generate_path(config, light_ray, 2)
    for this_intersect in path2
        object   = this_intersect.object
        distance = this_intersect.distance
        ray      = this_intersect.ray
        for i in 1:3
            e[i] *= object.colour[i]
        end
        light_contact_point = [0.0, 0.0, 0.0, 0.0]
        tmp = 0.999999999 * distance
        for i in 1:4
            light_contact_point[i] = ray.origin[i] + tmp * ray.direction[i]
        end
        light_normal = contact_normal(config, object, light_contact_point, this_intersect)

        push!(path, (object, light_contact_point, light_normal, e))
    end

    #println("E: ", [x[4] for x in path])
    return path
end

function generate_path(config::Config, ray::Ray, max_depth::Int64)
    # Generate the eye path starting from the given ray
    path_throughput = config.path_throughput
    contact_point = config.contact_point

    depth = 0
    path = Intersection[]
    while true
        depth += 1
        if depth > max_depth
            break
        end
        closest_intersect = intersect_scene(config, ray)::Intersection
        closest_object = closest_intersect.object
        distance = closest_intersect.distance

        # If ray hits nothing or a light, then stop
        if closest_object == nothing || closest_object.is_light
            break
        end

        # Lets add this intersection to the path
        push!(path, closest_intersect)

        # manually devectorize
        # contact_point = ray.origin + 0.999999999 * distance * ray.direction
        tmp = 0.999999999 * distance
        for i in 1:4
            contact_point[i] = ray.origin[i] + tmp * ray.direction[i]
        end
        normal = contact_normal(config, closest_object, contact_point, closest_intersect)

        if closest_object.specularity >= rand()
            wi, ipdf = sample_mirror(config, normal, ray.direction)
        else
            wi, ipdf = sample_diffuse(config, normal, ray.direction)
        end

        ray = Ray(contact_point, wi)
    end
    path
end

function radiance_bidirectional(config::Config, ray::Ray)
    # Setup memory
    Li = config.Li
    path_throughput = config.path_throughput
    contact_point = config.contact_point
    for i in 1:3
        Li[i] = 0.0
        path_throughput[i] = 1.0
    end

    # generate eye path
    light_path = generate_light_path(config)
    eye_path = generate_path(config, ray, config.maxdepth)

    # compute radiance
    for this_intersect in eye_path
        closest_object = this_intersect.object

        # contact_point = ray.origin + 0.999999999 * distance * ray.direction
        tmp = 0.999999999 * this_intersect.distance
        for i in 1:4
            contact_point[i] = ray.origin[i] + tmp * ray.direction[i]
        end

        colour = closest_object.colour
        normal = contact_normal(config, closest_object, contact_point, this_intersect)

        e = estimate_direct_lighting(config, contact_point, normal, light_path)

        for i in 1:3
            Li[i] += path_throughput[i] * e[i] * colour[i]
        end
        for i in 1:3
            path_throughput[i] *= colour[i] # for diffuse/specular felection " * ipdf" is always "* 1.0"
        end

    end
    return Li
end

function screen_to_world(config::Config, row::Real, col::Real)
    # convert the screen (row, col) location to a position in the world
    # remember: invert the y axis
    colfrac = (col / config.cols) - 0.5
    rowfrac = (row / config.rows) - 0.5
    # manually devectorize
    # world   = config.world_00 + colfrac * config.image_plane_x - rowfrac * config.image_plane_y
    world_point   = [0.0, 0.0, 0.0, 1.0]
    ray_direction = [0.0, 0.0, 0.0, 0.0]
    s = 0.0
    N = 2.5
    for i in 1:3
        world_point[i] = config.world_00[i] + colfrac * config.image_plane_x[i] * N - rowfrac * config.image_plane_y[i] * N
        tmp = world_point[i] - config.camera_eye[i]
        ray_direction[i] = tmp
        s += tmp * tmp
    end
    # manually devectorize
    # normalise!(world)
    s = sqrt(s)
    for i in 1:3
        ray_direction[i] /= s
    end
    world_point, ray_direction
end

function create_ray(config, row, col)
    world_point, ray_direction = screen_to_world(config, row, col)
    if config.dof
        # Depth of field
        new_eye = [0.0, 0.0, 0.0, 1.0]
        N = 0.1 # size of apeture
        for i in 1:3
            new_eye[i] = config.camera_eye[i] + N * (rand() - 0.5) * config.image_plane_x[i] +
                                                N * (rand() - 0.5) * config.image_plane_y[i] 
            world_point[i] -= new_eye[i]
        end
        world_point[4] = 0.0
        ray = Ray(new_eye, world_point)
    else
        ray = Ray(config.camera_eye, ray_direction)
    end
    ray
end

function render_pixel(config::Config, row::Int64, col::Int64)
    if config.sqrt_n_rays_per_pixel == 1
        ray = create_ray(config, row, col)::Ray
        pixel_colour = radiance_bidirectional(config, ray)
    else
        pixel_colour_accumulator = [0.0, 0.0, 0.0, 0.0]
        N = config.sqrt_n_rays_per_pixel * config.sqrt_n_rays_per_pixel  
        for sample in 1:N
            ray = create_ray(config, row + rand() - 0.5, col + rand() - 0.5)::Ray
            pixel_colour = radiance_bidirectional(config, ray)
            for i in 1:3
                pixel_colour_accumulator[i] += pixel_colour[i]
            end
        end
        pixel_colour = pixel_colour_accumulator / N
    end
    return pixel_colour
end

function render_row(row::Int64)
    [render_pixel(start_config, row, col) for col=1:start_config.cols]
end

function render(rows::Int64, cols::Int64)
    pmap(render_row, 1:rows)
end




