#!/usr/bin/env julia

require("Transform.jl")
require("Ray.jl")
require("Object.jl")
require("Config.jl")
require("Intersection.jl")

type Cube <: Object
    colour::Vec4d
    specularity::Float64
    refractive::Bool
    object2world::Transform
    world2object::Transform
    is_light::Bool
    Cube(colour::Vec4d, object2world::Transform) = new(colour, 0.0, false, object2world, -object2world, false)
    Cube(colour::Vec4d, specularity::Float64, object2world::Transform) = new(colour, specularity, false, object2world, -object2world, false)
    Cube(colour::Vec4d, refractive::Bool, object2world::Transform) = new(colour, 0.0, refractive, object2world, -object2world, false)
end

function intersect(config::Config, self::Cube, ray::Ray)
    # Use pre-allocated vectors, and do calculations inline
    ray_origin    = config.ray_origin
    ray_direction = config.ray_direction
    matrix_vector_mul!(self.world2object.mat, ray.origin   , ray_origin)
    matrix_vector_mul!(self.world2object.mat, ray.direction, ray_direction)
    ray_length = normalise_and_get_length!(ray_direction)

    tnear = -inf(Float64)
    tfar  =  inf(Float64)
    for i in 1:3
        swap = false
        if ray_direction[i] == 0.0
            # ray is parallel to this axis
            if abs(ray_origin[i]) > 0.5
                # and misses the cube entirely
                return nothing, nothing
            end
        else
            # Ray is not parallel to this axis
            t1 = (-0.5 - ray_origin[i]) / ray_direction[i]
            t2 = ( 0.5 - ray_origin[i]) / ray_direction[i]
            if t1 > t2
                t1, t2 = t2, t1
                swap = true
            end
            if t1 > tnear
                tnear = t1
                hit_swap  = swap
                hit_plane = i
            end
            if t2 < tfar
                tfar  = t2
            end
            if tnear > tfar
                # Box is missed
                return nothing, nothing
            end
            if tfar < 0
                # Box is behind ray_
                return nothing, nothing
            end
        end
    end
    return Intersection(self, ray, tnear/ray_length, true), Intersection(self, ray, tfar/ray_length, false)
end

function contact_normal(config::Config, self::Cube, contact_point::Vec4d, this_intersect::Intersection)
    object_normal = config.object_normal
    world_normal  = config.world_normal
    matrix_vector_mul!(self.world2object.mat, contact_point, object_normal)
    object_normal[4] = 0.0
    # Convert the contact point into a normal in one of the 6 possible directions
    # find the abs(maximum) value. 
    #    - Its index tells you the axis.
    #    - Its sign indicates the direction
    maxval = -inf(Float64)
    maxi    = 1
    maxsign = 1
    for i in 1:3
        if abs(object_normal[i]) > maxval
            maxval = abs(object_normal[i])
            maxi    = i
            maxsign = sign(object_normal[i])
        end
        object_normal[i] = 0.0
    end
    object_normal[maxi] = maxsign
    matrix_vector_mul!(self.object2world.mat, object_normal, world_normal)

    if this_intersect.entering
        return normalise!(world_normal)
    else
        return normalise!(neg!(world_normal))
    end
end

